<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppliedJob extends Model
{
    protected $table = 'applied';

    public function company()
    {
        return $this->belongsTo('App\User');
    }
}
