<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use Auth;
use App\AppliedJob;
class CompanyController extends Controller
{
    public function postJob(){
        return view('post-job');
    }

    /**
     * save posted job to DB
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveJob(Request $request){
    	$user_id = Auth::id();
    	 $this->validate($request, [
	        'title' => 'required',
	        'location' => 'required',
	        'job_type' => 'required',
	    ]);
    	$job = new Job();
    	$job->user_id = $user_id;
    	$job->title = $request->title;
    	$job->location = $request->location;
    	$job->job_type = $request->job_type;
    	$job->description = $request->description;
    	$job->howtoApply = $request->howtoApply;
    	$job->save();
    	return redirect('home');
    }

    /**
     * particular company posted job list
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postedJobsList(){
        $title = "Posted Jobs";
    	$user_id = Auth::id();
    	$jobs = Job::where('user_id',$user_id)->get();
    	return view('jobs',compact('jobs','title'));
    }

    /**
     * available jobs for users
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAvailablejobs(){
        $title = "Available Jobs";
        $user_id = Auth::id();
    	$applied_id = AppliedJob::where('user_id',$user_id)->pluck('job_id');
        $jobs = Job::whereNotIn('id',$applied_id)->get();
    	return view('jobs',compact('jobs','title'));
    }

    /**
     * user applied for this job
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function applyJob($id){
    	$user_id = Auth::id();
    	$applied = new AppliedJob();
    	$applied->user_id = $user_id;
    	$applied->job_id = $id;
    	$applied->save();
		return redirect('jobs')->with('status','Successfully Applied!!');
    }


    /**
     * users applied jobs
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function appliedJobs(){
        $title = "Applied Jobs";
    	$user_id = Auth::id();
    	$applied_ids = AppliedJob::where('user_id',$user_id)->pluck('job_id');
        $jobs = Job::whereIn('id',$applied_ids)->get();
        return view('jobs',compact('jobs','title'));
    }
}
