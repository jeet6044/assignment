<?php


function isActiveRoute($route, $output = 'active')
{
    if (Route::currentRouteName() == $route || Request::is($route)) {
        return $output;
    }
}
