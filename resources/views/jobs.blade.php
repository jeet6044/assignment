@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('sidebar')
            </div>
            <div class="col col-md-9">
                @if(Auth::user()->role == 1)
                    <h2 class="text-center">Posted Jobs</h2>
                    <a class="btn btn-primary pull-right" href="{{url('/post-a-job')}}">Post a Job</a>
                @else
                    <h2 class="text-center">Available Jobs</h2>
                @endif
                <table class="table table-striped">
                	<tr>
                        <th>Company Name</th>
                		<th>Title</th>
                		<th>Location</th>
                		<th>Job Type</th>
                		<th>Descripption</th>
                        @if(Auth::user()->role != 1) <th>Action</th> @endif
                	</tr>
                	@foreach($jobs as $job)
                		<tr>
                            <td>{{$job->company->name}}
                			<td>{{$job->title}}</td>
                			<td>{{$job->location}}</td>
                			<td>{{$job->job_type}}</td>
                			<td>{{$job->description}}</td>
                            <td>
                            @if(Auth::user()->role != 1)

                                <a class="btn btn-primary" href="{{url('apply/job/'.$job->id)}}">Apply</a>
                            @endif
                            </td>
                		</tr>
                	@endforeach
                </table>
            </div>
        </div>
    </div>
@endsection