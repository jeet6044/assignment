@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('sidebar')
            </div>
            <div class="col col-md-9">
                <h2 class="text-center">Post a Job</h2>
                <form method="post" action="{{url('save-job')}}">
                    {{ csrf_field() }}
                    <div class="form-group required @if($errors->has('job_role')) has-error @endif">
                        <label for="exampleInputEmail1" class='control-label'>Job Title</label>
                        <input type="text" name="title" class="form-control " id="exampleInputEmail1" aria-describedby="emailHelp"
                               placeholder="Ex: Senior Mobile Developer">
                        <span class="help-block m-b-none">@if($errors->has('job_role')) {{$errors->first('job_role')}} @endif</span>
                        
                    </div>
                    <div class="form-group required @if($errors->has('location')) has-error @endif">
                        <label for="exampleInputPassword1" class='control-label'>Location</label>
                        <input type="text" name="location" class="form-control" id="exampleInputPassword1"
                               placeholder="Ex:Hyderabad">
                        <span class="help-block m-b-none">@if($errors->has('location')) {{$errors->first('location')}} @endif</span>
                    </div>
                    <div class="form-group required @if($errors->has('job_type')) has-error @endif">
                        <label for="exampleSelect1" class='control-label'>Job Type</label>
                        <select class="form-control" id="exampleSelect1" name="job_type">
                            <option>Full Time</option>
                            <option>Part Time</option>
                            <option>Contract</option>
                        </select>
                        <span class="help-block m-b-none">@if($errors->has('job_type')) {{$errors->first('job_type')}} @endif</span>
                    </div>

                    <div class="form-group required">
                        <label for="exampleTextarea" class='control-label'>Job Description</label>
                        <textarea name="description" class="form-control" id="exampleTextarea" rows="7"></textarea>
                    </div>
                    <div class="form-group required">
                        <label for="exampleTextarea" class='control-label'>How to Apply</label>
                        <textarea name="howtoApply" class="form-control" id="exampleTextarea" rows="3"
                                  placeholder="For Example: Email your resume to jobs@xyz.com"></textarea>
                    </div>
                    <ul class="media-date text-uppercase reviews list-inline">
                        <li class="aaaa">
                            <button type="submit" class="btn btn-success">Publish</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
@endsection