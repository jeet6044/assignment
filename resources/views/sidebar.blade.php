<nav class="navbar navbar-default sidebar" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{isActiveRoute('/home') .' '. isActiveRoute('/')}}">
                    <a href="/home">Home</a>
                </li>
                @if(Auth::user()->role == 1)
                    <li class="{{isActiveRoute('/jobs')}}">
                        <a href="{{url('/jobs')}}">Posted Jobs</a>
                    </li>
                @else
                    <li class="{{isActiveRoute('/available-jobs')}}">
                        <a href="{{url('/available-jobs')}}">Available Jobs</a>
                    </li>
                    <li class="{{isActiveRoute('/applied-jobs')}}">
                        <a href="{{url('/applied-jobs')}}">Applied Jobs</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>