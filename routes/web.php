<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'admin'],function(){

	Route::get('/post-a-job','CompanyController@postJob');
	Route::get('/jobs','CompanyController@postedJobsList');
	Route::post('/save-job','CompanyController@saveJob');

});
Route::get('/available-jobs','CompanyController@getAvailablejobs');
Route::get('/apply/job/{id}','CompanyController@applyJob');
Route::get('/applied-jobs','CompanyController@appliedJobs');
